package com.example.infofilms.ghibli;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.infofilms.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GhibliActivity extends AppCompatActivity {
    ListView tv_list;
    ArrayList<Ghibli> ghibliFilms;
    GhibliAdapter adapter;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ghibli);
        tv_list = findViewById(R.id.tv_list);
        ghibliFilms = new ArrayList<Ghibli>();
        adapter = new GhibliAdapter(this, R.layout.row_ghibli, ghibliFilms);
        tv_list.setAdapter(adapter);
        checkConnection();
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Activity.CONNECTIVITY_SERVICE);
        assert connMgr != null;
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @SuppressLint("SetTextI18n")
    public void checkConnection() {
        if(isConnected()){
            dialog = ProgressDialog.show(GhibliActivity.this, "",
                    "Loading. Please wait...", true);
           getFromRetrofit();

        }else {
            Toast.makeText(this,"No hay conexión!", Toast.LENGTH_SHORT).show();
        }
    }
    public void getFromRetrofit(){
        MyService service = RetrofitClientInstance.getRetrofitInstance().create(MyService.class);
        Call<List<Ghibli>> call = service.getGhibli();
        call.enqueue(new Callback<List<Ghibli>>(){
            @Override
            public void onResponse(Call<List<Ghibli>> call, Response<List<Ghibli>> response) {
                List<Ghibli> value = response.body();
                ghibliFilms.addAll(value);
                adapter.notifyDataSetChanged();
                dialog.dismiss();

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFailure(Call<List<Ghibli>> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
