package com.example.infofilms.film;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.infofilms.R;
import java.util.ArrayList;

public class FilmAdapter extends ArrayAdapter<Film> {
    private int layoutResourceId;
    private Context context;
    private ArrayList<Film> data;

    public FilmAdapter(Context context, int layoutResourceId, ArrayList<Film> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        TextView tv_text = row.findViewById(R.id.text);
        TextView tv_film = row.findViewById(R.id.film);
        String Text = "Puntuación: "+ data.get(position).getPunctuation();
        String Film = "Pelicula: "+data.get(position).getTitle();

        Film film = data.get(position);

        tv_text.setText(Text);
        tv_film.setText(Film);

        if (film.getPunctuation()<2) {
            tv_text.setTextColor(context.getResources().getColor(R.color.colorRed));
        }
        else if (film.getPunctuation()>=4){
            tv_text.setTextColor(context.getResources().getColor(R.color.colorGreen));
        }
        else {
            tv_text.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }
        return row;

    }
}

