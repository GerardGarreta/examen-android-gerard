package com.example.infofilms.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.infofilms.film.Film;

import java.util.List;

@Dao
public interface FilmDao {
    @Query("SELECT * From film")
    List<Film> getFilms();
    @Query("SELECT * FROM film where id like :uuid")
    Film getFilm(String uuid);
    @Insert
    void addFilm(Film n);
    @Delete
    void deleteFilm(Film n);
    @Update
    void updateFilm(Film n);
}
