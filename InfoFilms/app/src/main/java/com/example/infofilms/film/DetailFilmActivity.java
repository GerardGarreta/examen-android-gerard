package com.example.infofilms.film;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.infofilms.R;
import com.example.infofilms.controller.FilmController;
import com.squareup.picasso.Picasso;

public class DetailFilmActivity extends AppCompatActivity {
    FilmController controller;
    Film film;
    TextView tv_id, tv_title, tv_description, tv_publicationYear, tv_punctuation;
    ImageView tv_urlImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);

        String id = getIntent().getStringExtra("idFilm");

        controller = FilmController.get(this);
        film = controller.getFilm(id);

        tv_id = findViewById(R.id.tv_id);
        tv_title = findViewById(R.id.tv_title);
        tv_description = findViewById(R.id.tv_description);
        tv_publicationYear = findViewById(R.id.tv_publicationYear);
        tv_punctuation = findViewById(R.id.tv_punctuation);
        tv_urlImg = findViewById(R.id.tv_urlImg);

        showFilm();
    }

    private void showFilm()
    {
        tv_id.setText(film.getId());
        tv_title.setText(film.getTitle());
        tv_description.setText(film.getDescription());
        tv_publicationYear.setText(String.valueOf(film.getPublicationYear()));
        tv_punctuation.setText(String.valueOf(film.getPunctuation()));
        Picasso.get().load(film.getUrlImg()).into(tv_urlImg);

    }

    public void deletePressed(View view) {
        controller.deleteFilm(film);
        finish();
    }
}