package com.example.infofilms.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.infofilms.R;

public class LoginActivity extends AppCompatActivity {
    EditText et_mail, et_contraseña;
    Button login;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_mail = findViewById(R.id.et_mail);
        et_contraseña = findViewById(R.id.et_contraseña);

        login = (Button) findViewById(R.id.btn_login);
        sp = getSharedPreferences("login", MODE_PRIVATE);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (et_mail.getText().toString().equals("")) {
                    et_mail.setError("El Email no puede ser vacío");
                }else if (et_contraseña.getText().toString().equals("")) {
                    et_contraseña.setError("La contraseña no puede ser vacia");
                } else {
                    sp.edit().putBoolean("logged", true).commit();
                    goToMainActivity();
                }
            }
        });
    }

    public void goToMainActivity() {
        finish();
    }
}

