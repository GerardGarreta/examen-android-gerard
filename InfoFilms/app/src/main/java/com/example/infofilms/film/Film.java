package com.example.infofilms.film;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.UUID;

@Entity(tableName = "film")
public class Film {
    @PrimaryKey
    @NonNull
    String id;
    String title;
    String description;
    int publicationYear;
    int punctuation;
    String urlImg;

    public Film(String title, String description, int publicationYear,int punctuation, String urlImg) {
        id = UUID.randomUUID().toString();
        this.title = title;
        this.description = description;
        this.publicationYear = publicationYear;
        this.punctuation = punctuation;
        this.urlImg = urlImg;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPublicationYear() {
        return this.publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public int getPunctuation() {
        return this.punctuation;
    }

    public void setPunctuation(int punctuation) {
        this.punctuation = punctuation;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }
}
