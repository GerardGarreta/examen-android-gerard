package com.example.infofilms.ghibli;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyService {

    @GET("films/")
    Call<List<Ghibli>> getGhibli();

}
