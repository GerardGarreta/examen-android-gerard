package com.example.infofilms.ghibli;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.infofilms.R;


import java.util.ArrayList;

public class GhibliAdapter extends ArrayAdapter<Ghibli> {
    private int layoutResourceId;
    private Context context;
    private ArrayList<Ghibli> data;

    GhibliAdapter(Context context, int layoutResourceId, ArrayList<Ghibli> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row_ghibli;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row_ghibli = inflater.inflate(layoutResourceId, parent, false);
        TextView txtName = (TextView) row_ghibli.findViewById(R.id.txtTitle);
        Ghibli g = data.get(position);
        txtName.setText(g.getValue());{
        }return row_ghibli;
    }
}
