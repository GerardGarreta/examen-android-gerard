package com.example.infofilms.ghibli;

class Ghibli {
    String id;
    String title;

    public Ghibli(String id, String value){
        this.id = id;
        this.title = value;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.title = value;
    }
}
