package com.example.infofilms.controller;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.infofilms.database.FilmDao;
import com.example.infofilms.database.FilmDatabase;
import com.example.infofilms.film.Film;

import java.util.List;

public class FilmController {

    private static FilmController controller;
    private FilmDao filmDao;

    private FilmController(Context context){
        Context appContext = context.getApplicationContext();
        FilmDatabase database = Room.databaseBuilder(appContext, FilmDatabase.class, "film")
                .allowMainThreadQueries().build();
        filmDao= database.getFilmDao();
    }

    public static FilmController get (Context context)
    {
        if (controller == null){
            controller = new FilmController(context);
        }
        return controller;
    }

    public List<Film> getFilms()
    {
        return filmDao.getFilms();
    }

    public Film getFilm(String id)
    {
        return filmDao.getFilm(id);
    }

    public void createFilm(Film f)
    {
        filmDao.addFilm(f);
    }

    public void deleteFilm(Film f)
    {
        filmDao.deleteFilm(f);
    }

    public void updateFilm(Film f)
    {
        filmDao.updateFilm(f);
    }
}
