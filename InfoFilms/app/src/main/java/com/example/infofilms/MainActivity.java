package com.example.infofilms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.infofilms.controller.FilmController;
import com.example.infofilms.film.AddFilmActivity;
import com.example.infofilms.film.DetailFilmActivity;
import com.example.infofilms.film.FilmAdapter;
import com.example.infofilms.ghibli.GhibliActivity;
import com.example.infofilms.film.Film;
import com.example.infofilms.login.LoginActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    FilmAdapter adapter;
    ArrayList<Film> films;
    FilmController controller;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        if(!sp.getBoolean("logged",false)){
            goToLoginActivity();
        }
        listView = findViewById(R.id.listview);
        films = new ArrayList<Film>();
        adapter = new FilmAdapter(this, R.layout.row, films);
        listView.setAdapter(adapter);
        controller = FilmController.get(this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(MainActivity.this, DetailFilmActivity.class);
            intent.putExtra("idFilm", films.get(position).getId());
            startActivity(intent);
        }
    });
}

    @Override
    protected void onResume() {
        super.onResume();
        showFilms();
    }

    private void showFilms() {
        films.clear();
        films.addAll(controller.getFilms());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addFilm:
                Intent intent = new Intent(MainActivity.this, AddFilmActivity.class);
                startActivity(intent);
                return (true);
        }
        switch (item.getItemId()) {
            case R.id.action_url:
                Intent intent = new Intent(MainActivity.this, GhibliActivity.class);
                startActivity(intent);
                return (true);
        }

            return (super.onOptionsItemSelected(item));
        }

    public void goToLoginActivity(){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

}




