package com.example.infofilms.film;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.infofilms.R;
import com.example.infofilms.controller.FilmController;


public class AddFilmActivity extends AppCompatActivity {

    EditText et_title, et_description, et_publicationYear, et_punctuation, et_urlImg;
    FilmController controller;
    Film film;
    String id;
    Button btn_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_film);

        et_title = findViewById(R.id.et_title);
        et_description = findViewById(R.id.et_description);
        et_publicationYear = findViewById(R.id.et_publicationYear);
        et_punctuation = findViewById(R.id. et_punctuation);
        et_urlImg = findViewById(R.id.et_urlImg);

        btn_create = findViewById(R.id.btn_create);
        controller = FilmController.get(this);

        id = getIntent().getStringExtra("idFilm");
        if (id != null) {
            film = controller.getFilm(id);
            showFilms();
        }
    }

    private void showFilms() {
        et_title.setText(film.getTitle());
        et_description.setText(film.getDescription());
        et_publicationYear.setText(film.getPublicationYear());
        et_punctuation.setText(film.getPunctuation());
        et_urlImg.setText(String.valueOf(film.getUrlImg()));
    }

    public void createPressed(View view) {
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        String publicationYear = et_publicationYear.getText().toString();
        String punctuation = et_punctuation.getText().toString();
        String urlImg = et_urlImg.getText().toString();


        if (checkFields(title, description, publicationYear, punctuation, urlImg)) {
            if (id != null) {
                film.setTitle(title);
                film.setDescription(description);
                film.setPublicationYear(Integer.parseInt(publicationYear));
                film.setPunctuation(Integer.parseInt(punctuation));
                film.setUrlImg(urlImg);

                controller.updateFilm(film);

            } else {
                Film f = new Film(title, description, Integer.parseInt(publicationYear), Integer.parseInt(punctuation), urlImg);
                controller.createFilm(f);
            }
            finish();
        }
    }

    private boolean checkFields(String title, String description, String publicationYear,String punctuation, String urlImg) {
        boolean valid = true;
        if ("".equals(title)) {
            et_title.setError("El Titulo no puede ser vacío");
            valid = false;
        }
        if ("".equals(description)) {
            et_description.setError("La Descripción no puede ser vacío");
            valid = false;
        }
        if ("".equals(publicationYear)) {
            et_publicationYear.setError("El año de publicación no puede ser vacío");
            valid = false;
        }
        if ("".equals(punctuation)) {
            et_punctuation.setError("El campo puntuación no puede ser vacío");
            valid = false;
        }else {
            int punt = Integer.parseInt(punctuation);
            if (punt!=0 && punt!=1 && punt!=2 && punt!=3 && punt!=4 && punt!=5){
                et_punctuation.setError("La puntuación tiene que ser entre 0 y 5");
                valid = false;
            }
        }
        if ("".equals(urlImg)) {
            et_urlImg.setError("El campo de URL no puede ser vacío");
            valid = false;
        }
        return valid;
    }
}